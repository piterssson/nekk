<?php

namespace AppBundle\Services;

use AppBundle\Entity\Task;

class PercentageProject {

    private $doctrine;

    /**
     * PercentageProject constructor.
     * @param $doctrine
     */
    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param $id
     * @return float|int
     */
    public function count($id) {

        $countAll = $this->doctrine->getRepository(Task::class)->countAll($id);
        $countDone = $this->doctrine->getRepository(Task::class)->countDone($id);

        if ($countDone != 0) {
            return ($countDone/$countAll) * 100;
        }
        return 0;
    }

}