<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Project;
use AppBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class ProjectController extends Controller
{
    private $templating;

    private $doctrine;

    /**
     * ProjectController constructor.
     * @param EngineInterface $templating
     * @param $doctrine
     */
    public function __construct(EngineInterface $templating, $doctrine)
    {
        $this->templating = $templating;
        $this->doctrine = $doctrine;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $projects = $this->doctrine->getRepository(Project::class)->findAll();
        return $this->templating->renderResponse('AppBundle::projects.html.twig', array('projects' => $projects));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {
        $tasks = $this->doctrine->getRepository(Task::class)->findByProject($id);
        return $this->templating->renderResponse('AppBundle::project.html.twig', array('tasks' => $tasks));
    }
}