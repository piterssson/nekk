<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Task;

class TaskController extends Controller
{
    private $templating;

    private $doctrine;

    /**
     * TaskController constructor.
     * @param EngineInterface $templating
     * @param $doctrine
     */
    public function __construct(EngineInterface $templating, $doctrine)
    {
        $this->templating = $templating;
        $this->doctrine = $doctrine;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {
        $task = $this->doctrine->getRepository(Task::class)->findOneByProject($id);
        return $this->templating->renderResponse('AppBundle::task.html.twig', array('task' => $task));
    }
}
